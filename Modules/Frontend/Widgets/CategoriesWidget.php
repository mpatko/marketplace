<?php

namespace Modules\Frontend\Widgets;

use App\Models\Category;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;

/**
 * Class CategoriesWidget
 *
 * @package Modules\Frontend\Widgets
 */
class CategoriesWidget extends AbstractWidget
{
    /**
     * Run widget.
     *
     * @return View
     */
    public function run(): View
    {
        $categories = Category::getTree();

        return view('frontend::widgets.categories', [
            'data' => $categories
        ]);
    }
}
