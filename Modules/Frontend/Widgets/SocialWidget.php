<?php

namespace Modules\Frontend\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Models\Setting;
use Illuminate\View\View;

/**
 * Class SocialWidget
 *
 * @package Modules\Frontend\Widgets
 */
class SocialWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Run widget.
     *
     * @return View
     */
    public function run(): View
    {
        return view('frontend::widgets.social', [
            'facebook' => Setting::getValue('facebook_link'),
            'instagram' => Setting::getValue('instagram_link'),
            'youtube' => Setting::getValue('youtube_link'),
            'twiter' => Setting::getValue('twitter_link'),
        ]);
    }
}
