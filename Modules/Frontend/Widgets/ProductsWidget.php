<?php

namespace Modules\Frontend\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Models\Product;
use Illuminate\View\View;

/**
 * Class ProductsWidget
 *
 * @package Modules\Frontend\Widgets
 */
class ProductsWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'limit' => 12,
        'sortBy' => 'updated_at',
        'sortDir' => 'desc',
        'title' => null
    ];

    /**
     * Run widget.
     *
     * @return View
     */
    public function run(): View
    {
        $products = Product::orderBy($this->config['sortBy'], $this->config['sortDir'])
            ->limit($this->config['limit'])
            ->get();

        return view('frontend::widgets.products', [
            'data' => $products,
            'config' => $this->config
        ]);
    }
}
