const productModule = {
  /**
   * Initialize module.
   * @param settings - Settings to override default.
   */
  __init: function (settings) {
    productModule._config = {
      /**
       * Selector for slick slider main img.
       */
      slickSliderMainImg: '.slider-for',

      /**
       * Selector for slick slider navigation.
       */
      slickSliderNav: '.slider-nav',

      /**
       * Configuration object for slick slider main img.
       */
      configSliderMainImg: {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrow: false,
        fade: true,
        asNavFor: '.slider-nav',
      },

      /**
       * Configuration object for slick slider navigation.
       */
      configSliderNav: {
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        focusOnSelect: true,
      },

      /**
       * Selector for decrement quantity btn.
       */
      decrementQuantitySelector: '.quantity-left-minus',

      /**
       * Selector for increment quantity btn.
       */
      incrementQuantitySelector: '.quantity-right-plus',

      /**
       * Selector for quantity input.
       */
      quantityInputSelector: '.quantity',
    };

    // Allow overriding the default config
    $.extend(productModule._config, settings);

    productModule._setup();
  },

  /**
   * Run application, add listeners
   */
  _setup: function () {

    $(productModule._config.slickSliderMainImg).slick(
        productModule._config.configSliderMainImg
    );
    $(productModule._config.slickSliderNav).slick(
        productModule._config.configSliderNav
    );

    /**
     * Even listeners.
     */
    // Click on quantity minus button.
    $('body').on('click', productModule._config.decrementQuantitySelector, productModule.quantityDecrement)

    // Click on quantity plus button.
        .on('click', productModule._config.incrementQuantitySelector, productModule.quantityIncrement)
  },

  /**
   * Decrement quantity.
   */
  quantityDecrement: function (e) {
    e.preventDefault();
    const quantity = parseInt($(productModule._config.quantityInputSelector).val());
    if (quantity > 1) {
      $(productModule._config.quantityInputSelector).val(quantity - 1);
    }
  },

  /**
   * Increment quantity.
   */
  quantityIncrement: function (e) {
    e.preventDefault();
    const quantity = parseInt($(productModule._config.quantityInputSelector).val());
    $(productModule._config.quantityInputSelector).val(quantity + 1);
  },
};
