const cartModule = {
  /**
   * Initialize module.
   * @param settings - Settings to override default.
   */
  __init: function (settings) {
    cartModule._config = {
      /**
       * Selector for add product to cart btn.
       */
      addToCartBtnSelector: '.add-to-cart-btn',

      /**
       * Selector for number of items in the cart.
       */
      itemsInCartSelector: '.cart-badge',

      /**
       * Selector for quantity input.
       */
      quantityInputSelector: '.quantity',

      /**
       * Url for add product in cart
       */
      urlAddProduct: '/product/cart',
    };

    // Allow overriding the default config
    $.extend(cartModule._config, settings);

    cartModule._setup();
  },

  /**
   * Run application, add listeners
   */
  _setup: function () {
    /**
     * Calculates the quantity of products in the cart
     */
    cartModule.cartBadge();

    /**
     * Even listeners.
     */
    //Click on add to cart button
    $('body').on('click', cartModule._config.addToCartBtnSelector, cartModule.addProduct);
  },

  /**
   * Add product to cart event.
   *
   * @param e
   */
  addProduct: function (e) {
    const product_id = $(e.target).data('product-id');
    if (cartModule.checkCartAvailability(product_id)) {
      cartModule.openCartModalWindow();
    } else {
      cartModule.addProductToCartItemsCookie(product_id)
    }
  },

  /**
   * Add product to cart.
   *
   * @param product_id
   */
  addProductToCartItemsCookie: function (product_id) {
    $.ajax({
      url: cartModule._config.urlAddProduct,
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: "POST",
      data: {id: product_id, quantity: $(cartModule._config.quantityInputSelector).val()},
      success: function () {
        cartModule.cartBadge();
        $(cartModule._config.addToCartBtnSelector).text(commonModule.trans('frontend::product.desc.already_in_cart'));
      }
    });
  },

  /**
   * Open cart nodal window
   */
  openCartModalWindow: function () {
    alert('MODAL');
  },

  /**
   * Set cart number badge
   */
  cartBadge: function () {
    const count = $.cookie("cart_items") ? JSON.parse($.cookie("cart_items")).length : 0;
    $(cartModule._config.itemsInCartSelector).text(count);
  },

  /**
   * Check if the product is in the cart.
   *
   * @param product_id
   * @returns {boolean}
   */
  checkCartAvailability: function (product_id) {
    let cart_items = [];
    if ($.cookie('cart_items')) {
      let cookie = JSON.parse($.cookie('cart_items'));
      cart_items = cookie.filter(function (el) {
        return +el.id === +product_id;
      });
    }
    return !!cart_items.length;
  },
};
