const categoriesModule = {
  /**
   * Initialize module.
   * @param settings - Settings to override default.
   */
  __init: function (settings) {
    categoriesModule._config = {
      /**
       * Categories list item selector
       */
      categoriesWidgetLiSelector: '.categories-widget .widget-content li',

      /**
       * Subcategory list selector
       */
      subcategoryListSelector: '.subcategory-list'
    };

    // Allow overriding the default config
    $.extend( categoriesModule._config, settings );

    // Temp storage
    categoriesModule._storage = {};

    categoriesModule._setup();
  },

  /**
   * Run application, add listeners
   */
  _setup: function() {
    if ($(categoriesModule._config.categoriesWidgetLiSelector).length) {
      $('body')
        .on('mouseover', categoriesModule._config.categoriesWidgetLiSelector, categoriesModule.openModal)
        .mouseover(categoriesModule.hoverAnywhere);
    }

  },

  /**
   * Open categories modal.
   *
   * @param event
   */
  openModal: function(event) {
    const $li = $(this);
    const $ul = $li.closest('ul');
    const modalId = $ul.data('target');
    const categoryId = $li.data('id');
    $ul.find('li').removeClass('active');
    $li.addClass('active');
    const $modal = $('#' + modalId);
    categoriesModule._storage.$modal = $modal;
    $modal.find(categoriesModule._config.subcategoryListSelector + '[data-parent!=' + categoryId + ']').hide();
    $modal.find(categoriesModule._config.subcategoryListSelector + '[data-parent=' + categoryId + ']').show();
    $modal.show();
  },

  /**
   * mouse over on anywhere.
   *
   * @param event
   */
  hoverAnywhere: function(event) {
    $li = $(categoriesModule._config.categoriesWidgetLiSelector);
    if (categoriesModule._storage.$modal &&
      !categoriesModule._storage.$modal.is(event.target) &&
      categoriesModule._storage.$modal.has(event.target).length === 0 &&
      $li &&
      !$li.is(event.target) &&
      $li.has(event.target).length === 0)
    {
      categoriesModule._storage.$modal.hide();
      $activeLi = $(categoriesModule._config.categoriesWidgetLiSelector + '.active');
      if ($activeLi.length) {
        $activeLi.removeClass('active');
      }
    }
  }
};
