const commonModule = {
  /**
   * Initialize module.
   * @param settings - Settings to override default.
   */
  __init: function (settings) {
    commonModule._config = {
      /**
       * Jquery selector of labguage selector.
       */
      $languageSelector: $('.language_drop'),

      /**
       * Jquery selector of scrollbar container
       */
      $scollBoxSelector: $('.scrollbox')
    };

    // Allow overriding the default config
    $.extend(commonModule._config, settings);

    commonModule._setup();
  },

  /**
   * Run application, add listeners
   */
  _setup: function () {
    // Language selector.
    commonModule._config.$languageSelector.msDropdown({roundedBorder: false});

    // mCustomScrollbar.
    commonModule._config.$scollBoxSelector.mCustomScrollbar();
  },

  /**
   * Translate message.
   *
   * @param key
   * @param replace
   * @returns {*}
   */
  trans: function (key, replace = {}) {
    const key_parts = key.split('::');
    if (key_parts.length === 1) {
      key = key_parts[0];
      translations = window.global_translations;
    } else {
      key = key_parts[1];
      translations = window.frontend_translations;
    }
    let translation = key.split('.').reduce((t, i) => t[i] || null, translations);

    for (let placeholder in replace) {
      translation = translation.replace(`:${placeholder}`, replace[placeholder]);
    }

    return translation;
  },
};
