<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{--<meta http-equiv="content-language" content="{{ $currentLocale }}">--}}
    <title>BUM Website</title>

    {{-- Laravel Mix - CSS File --}}
    <link rel="stylesheet" href="{{ mix('frontend/css/vendor.css') }}">
    <link rel="stylesheet" href="{{ mix('frontend/css/app.css') }}">
  </head>
  <body>
  <div class="wrapper">
    @include('frontend::includes.header')
    <div class="main-section">
      @include('frontend::includes.sidebar')
      @yield('content')
    </div>
    @include('frontend::includes.footer')
  </div>

    <script>
      window.global_translations = {!! Cache::get('global_translations_' . $currentLocale) !!};
      window.frontend_translations = {!! Cache::get('frontend_translations_' . $currentLocale) !!};
    </script>
    <script src="{{ mix('frontend/js/app.js') }}"></script>
  </body>
</html>
