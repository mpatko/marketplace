@extends('frontend::layouts.master')

@section('content')
	<div class="product-detail-content">
		<div class="row">
			<div class="col-md-5">
				<div class="img-box">
					<div class="slider-for">
						<div class="">
							<img src="{{ count($product->images) ? asset('storage/'. $product->images[0]->path) : '' }}" class="img-fluid">
						</div>
						<div class="">
							<img src="{{ count($product->images) ? asset('storage/'. $product->images[0]->path) : '' }}" class="img-fluid">
						</div>
						<div class="">
							<img src="{{ count($product->images) ? asset('storage/'. $product->images[0]->path) : '' }}" class="img-fluid">
						</div>
						<div class="">
							<img src="{{ count($product->images) ? asset('storage/'. $product->images[0]->path) : '' }}" class="img-fluid">
						</div>
					</div>
                    <div class="slider-nav">
						<div>
                        	<img src="{{ count($product->images) ? asset('storage/'. $product->images[0]->path) : '' }}">
						</div>
						<div>
							<img src="{{ count($product->images) ? asset('storage/'. $product->images[0]->path) : '' }}">  
						</div>
						<div>
							<img src="{{ count($product->images) ? asset('storage/'. $product->images[0]->path) : '' }}">  
						</div>
						<div>
							<img src="{{ count($product->images) ? asset('storage/'. $product->images[0]->path) : '' }}">  
						</div>
					</div>
				</div>
            </div>
			<div class="col-md-7">
				<div class="desc">
					<div class="product-label">
						<span>New</span>
						<span class="sale">-20%</span>
					</div>
					<h3 class="title">{{$product->title}}</h3>
					<div class="price-block">
						<h5 id="price">
							{{$product->currency->sign}}{{$product->price}}
						</h5>
						<p class="rate">
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							(74 Rating)
						</p>
					</div>
					<div>
						<p>@lang('frontend::product.desc.category'): {{$product->category->title}}</p>
					</div>
					<hr>
					<div>
						<p>{{$product->in_store}} @lang('frontend::product.desc.in_store')</p>
					</div>
					<hr>
					<div>
						<p>{{$product->description}}</p> 
					</div>
					<hr>
					<div class="product-options">
						<ul class="size-option">
							<li><span class="text-uppercase">Size:</span></li>
							<li class="active"><a href="#" class="icon-animate">S</a></li>
							<li><a href="#" class="icon-animate">XL</a></li>
							<li><a href="#" class="icon-animate">SL</a></li>
						</ul>
						<ul class="color-option">
							<li><span class="text-uppercase">Color:</span></li>
							<li class="active icon-animate"><a href="#" style="background-color:#475984;"></a></li>
							<li class="icon-animate"><a href="#" style="background-color:#8A2454;"></a></li>
							<li class="icon-animate"><a href="#" style="background-color:#BF6989;"></a></li>
							<li class="icon-animate"><a href="#" style="background-color:#9A54D8;"></a></li>
						</ul>
					</div>
					<div class="d-flex justify-content-between">
						<div class="d-flex order">
							<span class="input-group-btn">
								<button type="button" class="quantity-left-minus quantity-btn btn"  data-type="minus">
									<span>-</span>
								</button>
							</span>
							<input type="number" class="quantity" name="quantity" value="1">
							<span class="input-group-btn">
								<button type="button" class="quantity-right-plus quantity-btn btn" data-type="plus">
									<span>+</span>
								</button>
							</span>
							<button class="btn add-to-cart-btn" data-product-id="{{$product->id}}">
								@if($in_cart)
									@lang('frontend::product.desc.already_in_cart')
								@else
									@lang('frontend::product.desc.add_to_cart')
								@endif
							</button>
						</div>
						<div>
							<button class="icon-btn icon-animate"><i class="fa fa-heart"></i></button>
							<button class="icon-btn icon-animate"><i class="fa fa-exchange"></i></button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 tabulation">
				<ul class="nav nav-tabs">
					<li class="nav-item"><a data-toggle="tab" href="#description" class="nav-link show active">Опис</a></li>
					<li class="nav-item"><a data-toggle="tab" href="#manufacturer" class="nav-link">Характеристики</a></li>
					<li class="nav-item"><a data-toggle="tab" href="#review" class="nav-link">Відгуки</a></li>
				</ul>
				<div class="tab-content">
					<div id="description" class="tab-pane fade show active">
						<p>description</p>
					</div>
					<div id="manufacturer" class="tab-pane fade">
						<p>manufacturer</p>
					</div>
					<div id="review" class="tab-pane fade">
						<p>Rewiew</p>
					</div>
				</div>
			</div>
		</div>
	</div>	
@stop