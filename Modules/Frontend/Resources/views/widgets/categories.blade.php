<div class="widget sidebar-widget categories-widget">
  <div class="widget-title">
    <a href="#">@lang('frontend::widget_categories.all')</a>
  </div>
  <div class="widget-content">
    <ul data-target="categories_modal">
      @foreach($data as $category)
        <li data-id="{{ $category->id }}"><a href="#">{{ $category->title }}</a></li>
      @endforeach
    </ul>
  </div>

  <div id="categories_modal" class="categories-modal scrollbox">
    <div class="block-container">
      <div class="row">
        @foreach($data as $category)
          @foreach($category->children as $subcategory)
            <div class="col-4 subcategory-list" data-parent="{{ $category->id }}">
              <div class="cat-title">
                <a href="#">{{ $subcategory->title }}</a>
              </div>
              <ul>
                @foreach($subcategory->children as $subsubcategory)
                  <li><a href="#">{{ $subsubcategory->title }}</a></li>
                @endforeach
              </ul>
            </div>
          @endforeach
        @endforeach
      </div>
    </div>
  </div>
</div>