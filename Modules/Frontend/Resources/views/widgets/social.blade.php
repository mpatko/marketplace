<div class="widget sidebar-widget social-widget">
    <div class="widget-title">@lang('frontend::widget_follow_us.title')</div>
    <div class="widget-content">
      <ul class="social-links">
        <li>
          <a href="{{$facebook}}"><i class="fa fa-facebook-square fb-icon"></i></a>
        </li>
        <li>
          <a href="{{$instagram}}"><i class="fa fa-instagram insta-icon"></i></a>
        </li>
        <li>
          <a href="{{$youtube}}"><i class="fa fa-youtube yt-icon"></i></a>
        </li>
        <li>
          <a href="{{$twiter}}"><i class="fa fa-twitter-square tw-icon"></i></a>
        </li>
      </ul>
    </div>
</div>