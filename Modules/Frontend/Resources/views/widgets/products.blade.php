<div class="widget widget-latest-products product-grid">
  @if ($config['title'])
    <div class="widget-title">
      <h2>{{ $config['title'] }}</h2>
    </div>
  @endif
  <div class="widget-content">
    <div class="row">
      @foreach ($data as $key => $item)
        <div class="col widget-item">
          <div class="product-item">
            <div class="product-image">
              <a href="{{ LaravelLocalization::getLocalizedURL(null, "/product/{$item->id}") }}">
                <img src="{{ count($item->images) ? asset('storage/'. $item->images[0]->path) : '' }}" class="cover" alt="">
              </a>
            </div>
            <div class="product-footer">
              <ul>
                <li class="product-icon"><a href="#"><i class="icon_piechart"></i></a></li>
                <li><a class="btn-add-to-cart" href="#">@lang('frontend::order.add_to_cart')</a></li>
                <li class="product-icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
              </ul>
              <h4>{{ $item->title }}</h4>
              <h5>{{ $item->currency->sign }}{{ $item->price }}</h5>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
</div>