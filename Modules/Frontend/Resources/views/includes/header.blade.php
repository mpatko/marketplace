<header class="header s-container">
  <div class="left-side logo-area d-flex align-items-center">
    <img class="logo" src="frontend/img/logo.png" />
  </div>
  <div class="nav-area">
    <div class="row">
      <div class="col col-7">
        <ul class="account_list">
          <li class="dropdown">
            <a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="flag-icon flag-icon-ua mr-2"></i> UA
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-ru mr-2"></i> RU</a>
              <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-us mr-2"></i> EN</a>
            </div>
          </li>
          <li><a href="#">Menu 1</a></li>
          <li><a href="#">Menu 2</a></li>
          <li><a href="#">Menu 3</a></li>
          <li><a href="#">Menu 4</a></li>
          <li><a href="#">Menu 5</a></li>
        </ul>
        <div class="d-flex">
          <div class="advanced_search_area d-flex mr-3">

            <select class="selectpicker">
              <option>All categories</option>
              <option>Category 1</option>
              <option>Category 2</option>
              <option>Category 3</option>
            </select>
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Search" aria-label="Search">
              <span class="input-group-btn input-group-append">
              <button class="btn btn-search btn-red" type="button"><i class="icon-magnifier icons"></i></button>
            </span>
            </div>
          </div>
          <button class="btn btn-red">Create Product</button>
        </div>
      </div>
      <div class="col col-5">

        <ul class="d-flex icon-nav">
          <li class="text-center mr-3 d-flex flex-column">
            <span aria-hidden="true" class="icon icon-heart"></span>
            <div class="label">My wishes</div>
          </li>

          <li class="text-center mr-3 d-flex flex-column">
            <span aria-hidden="true" class="icon icon-chart"></span>
            <div class="label">Compare products</div>
          </li>
          <li class="text-center mr-3 d-flex flex-column">
            <span aria-hidden="true" class="icon icon-basket"></span>
            <div class="label">Cart</div>
            <span class="bdg bdg-red bdg-top-right bdg-small cart-badge"></span>
          </li>
          <li class="text-center mr-3 d-flex flex-column">
            <span aria-hidden="true" class="icon icon-bell"></span>
            <div class="label">Notifications</div>
          </li>
          <li class="text-center d-flex flex-column">
            <span aria-hidden="true" class="icon icon-user"></span>
            <div class="label">Sign In | Sign Up</div>
          </li>

        </ul>
      </div>
    </div>
  </div>
</header>