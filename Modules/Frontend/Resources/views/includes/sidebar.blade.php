<div class="left-side sidebar">
  @widget('Modules\Frontend\Widgets\CategoriesWidget')

  @widget('Modules\Frontend\Widgets\SocialWidget')

  <div class="widget sidebar-widget subscribe-widget">
    <div class="widget-title">@lang('frontend::widget_subscription.title')</div>
    <div class="widget-content">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="E-mail" aria-label="E-mail">
        <span class="input-group-btn input-group-append">
          <button class="btn btn-search btn-red" type="button"><i class="fa fa-envelope"></i></button>
        </span>
      </div>
      <p>@lang('frontend::widget_subscription.help')</p>
    </div>
  </div>
</div>