<footer>
  <div class="s-container footer-navs">
    <div class="row">
      <div class="col-lg-4 col-md-4 col-6">
        <div class="column about">
          <h2>Best Ukrainian Market</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
      </div>
      <div class="col-lg-2 col-md-4 col-6">
        <div class="column">
          <h3>Information</h3>
          <ul>
            <li><a href="#">About us</a></li>
            <li><a href="#">Delivery information</a></li>
            <li><a href="#">Terms & Conditions</a></li>
            <li><a href="#">Our policies</a></li>
            <li><a href="#">Disputes & complaints</a></li>
            <li><a href="#">Site map</a></li>
          </ul>
        </div>
      </div>
      <div class="col-lg-2 col-md-4 col-6">
        <div class="column">
          <h3>Help Center</h3>
          <ul>
            <li><a href="#">Contact us</a></li>
            <li><a href="#">Orders & returns</a></li>
            <li><a href="#">FAQ</a></li>
            <li><a href="#">Seller guide</a></li>
            <li><a href="#">Customer guide</a></li>
            <li><a href="#">Resolution center</a></li>
          </ul>
        </div>
      </div>

      <div class="col-lg-2 col-md-4 col-6">
        <aside class="column">
          <h3>My Account</h3>
          <ul>
            <li><a href="#">My account</a></li>
            <li><a href="#">Recently viewed products</a></li>
            <li><a href="#">Order History</a></li>
            <li><a href="#">Track my order</a></li>
            <li><a href="#">Wish List</a></li>
            <li><a href="#">Compare products</a></li>
          </ul>
        </aside>
      </div>
      <div class="col-lg-2 col-md-4 col-6">
        <div class="column">
          <h3>Extras</h3>
          <ul>
            <li><a href="#">Why work with us</a></li>
            <li><a href="#">Top shops</a></li>
            <li><a href="#">Popular products</a></li>
            <li><a href="#">Specials</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="s-container copyright">
    Copyright © {{ date('Y') }} Best Ukrainian Market. All rights reserved.
  </div>
</footer>