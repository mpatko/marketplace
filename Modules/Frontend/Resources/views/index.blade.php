@extends('frontend::layouts.master')

@section('content')
  <div class="content">
    @widget('Modules\Frontend\Widgets\ProductsWidget', [
      'limit' => 12,
      'sortBy' => 'updated_at',
      'sortDir' => 'desc',
      'title' => trans('frontend::widget_latest_products.title')
    ])
  </div>
@stop