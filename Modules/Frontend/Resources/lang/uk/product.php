<?php
/**
 * Translation for product-detail (UK).
 */

return [
    'desc' => [
        'category' => 'Категорія',
        'in_store' => 'елементів в магазині',
        'add_to_cart' => 'Додати в кошик',
        'already_in_cart' => 'Вже у кошику',
    ]
    
];
