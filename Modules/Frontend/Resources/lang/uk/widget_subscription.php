<?php
/**
 * Translation for Subscription widget (UK).
 */

return [
    'title' => 'Підписатись',
    'help' => 'Зареєструватись для отримання акцій та оновлень'
];
