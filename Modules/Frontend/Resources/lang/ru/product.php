<?php
/**
 * Translation for product-detail (RU).
 */

return [
    'desc' => [
        'category' => 'Категория',
        'in_store' => 'элементов в магазине',
        'add_to_cart' => 'Добавить в корзину',
        'already_in_cart' => 'Уже в корзине',
    ]
    
];
