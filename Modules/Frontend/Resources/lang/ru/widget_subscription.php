<?php
/**
 * Translation for Subscription widget (RU).
 */

return [
    'title' => 'Подписаться',
    'help' => 'Зарегистрироваться для получения акций и обновлений'
];
