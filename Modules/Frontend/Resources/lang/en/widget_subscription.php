<?php
/**
 * Translation for Subscription widget (EN).
 */

return [
    'title' => 'Subscribe',
    'help' => 'Register to get promotions and updates'
];
