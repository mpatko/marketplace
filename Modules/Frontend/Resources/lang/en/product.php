<?php
/**
 * Translation for product-detail (EN).
 */

return [
    'desc' => [
        'category' => 'Category',
        'in_store' => 'items in the store',
        'add_to_cart' => 'Add to cart',
        'already_in_cart' => 'Already in cart',
    ]
    
];
