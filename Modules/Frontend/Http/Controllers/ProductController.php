<?php

namespace Modules\Frontend\Http\Controllers;

use App\Models\Product;
use Modules\Frontend\Services\CartService;
use Illuminate\View\View;


class ProductController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        return view('frontend::product.index');
    }

    /**
     * Show the specified resource.
     *
     * @param Product $product
     * @param CartService $cartService
     * @return View
     */
    public function show(Product $product, CartService $cartService)
    {
        return view('frontend::product.show', [
            'product' => $product,
            'in_cart' => $cartService->checkIfProductInCart($product->id),
        ]);
    }
}
