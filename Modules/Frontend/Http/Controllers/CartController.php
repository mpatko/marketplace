<?php

namespace Modules\Frontend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Frontend\Services\CartService;

class CartController extends BaseController
{
    protected $cartService;

    public function __construct(CartService $cartService)
    {
        parent::__construct();
        $this->cartService = $cartService;
    }

    /**
     * Add product to cookie.
     *
     * @param Request $request
     * @return Response
     */
    public function addProduct(Request $request)
    {
        return $this->cartService->addProduct($request->get('id'), $request->get('quantity'));
    }
}
