<?php

namespace Modules\Frontend\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;

/**
 * Class BaseController
 * @package Modules\Admin\Http\Controllers
 */
class BaseController extends Controller
{
    /**
     * Current locale.
     *
     * @var string
     */
    protected $locale;

    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $this->locale = App::getLocale();
        $this->cacheTranslations('global_translations_' . $this->locale, resource_path('lang/' . $this->locale));
        $frontendPath = __DIR__ .'/../../Resources/lang/' . $this->locale;
        $this->cacheTranslations('frontend_translations_' . $this->locale, $frontendPath, 'frontend');

        View::share('currentLocale', $this->locale);
    }

    /**
     * Cache translations.
     *
     * @param $variableName
     * @param $path
     * @param null $prefix
     */
    private function cacheTranslations($variableName, $path, $prefix = null)
    {
        Cache::rememberForever($variableName, function () use ($path, $prefix) {
            return collect(File::allFiles($path))->flatMap(function ($file) use ($prefix) {
                return [
                    ($translation = $file->getBasename('.php')) =>
                        trans($prefix ? "$prefix::$translation" : $translation),
                ];
            })->toJson();
        });
    }
}
