<?php

namespace Modules\Frontend\Services;

use Illuminate\Http\Response;

class CartService
{
    /**
     * Check if the product is in the cart.
     *
     * @param $product_id
     * @return bool
     */
    public function checkIfProductInCart($product_id)
    {
        if (array_key_exists('cart_items', $_COOKIE)) {
            foreach (json_decode($_COOKIE['cart_items']) as $item) {
                if ($item->id == $product_id) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Add product to cookie.
     *
     * @param $id
     * @param $quantity
     * @return Response
     */
    public function addProduct($id, $quantity)
    {
        if (array_key_exists('cart_items', $_COOKIE)) {
            // Get products from cookie
            $products = json_decode($_COOKIE['cart_items'], true);
        }
        // Add new products to array
        $products[] = array('id' => $id, 'quantity' => $quantity);
        // Encode cookie
        $cookie = json_encode($products);
        // Add products to cookie
        return response('Set Cookie')
            ->cookie('cart_items', $cookie, 0, null, null, false, false);
    }
}
